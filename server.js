const express = require('express')

const app = express();

const mongoose = require('mongoose');


mongoose.connect('mongodb://127.0.0.1:27017/crud');

app.use(express.json())

const routerEmployee = require('./routes/Employee')


app.use(routerEmployee)

app.listen(4000, '0.0.0.0',() => {
    console.log('server started on port 4000');
})