// import mongoose
const mongoose = require('mongoose')

// create a model schema
const EmployeeSchema = new mongoose.Schema({
  name: String,
  email: String,
  department: String,
 
})

// create a model (collection) named User
// Note:
// - model name: User => collection name: users
module.exports = mongoose.model('Employee', EmployeeSchema)