
const express = require('express')

const Employee = require('../models/Employee')

const utils = require('../utils')

const router = express.Router();

router.post('/employee/add', (request, response) => {

    const { name, email, department } = request.body

    const employee = new Employee()

    employee.name = name
    employee.email = email
    employee.department = department

    employee.save((error, result) => {
        // console.log(result);
        response.send(utils.createResult(error, result))
    })

})

router.get('/employee/all', (request, response) => {
    Employee.find()

        .populate('name email department')
        .exec((error, employees) => {
            response.send(utils.createResult(error, employees))
        })
    // response.send('Get all employee')
})

router.put('/employee/update/:id', (request, response) => {

    const { id } = request.params

    const { name, email, department } = request.body

    Employee.findOne({ _id: id }).exec((error, employee) => {

        employee.name = name
        employee.email = email
        employee.department = department

        employee.save((error, updateUser) => {
            // console.log(result)
            // response.send('update User Successfully')

            response.send(utils.createResult(error, updateUser))
        })
    })

})

router.delete('/employee/delete/:id', (request, response) => {

    const { id } = request.params

    Employee.deleteOne({ _id: id }).exec((error,employee) => {

        response.send(utils.createResult(error, employee))
    })


})
// response.send('Delete Employee')


router.get('/employee/:id', (request, response) => {
    const { id } = request.params

    // find the user with id
    Employee.findOne({ _id: id }).exec((error, employee) => {
        response.send(utils.createResult(error, employee))
    })
})

module.exports = router